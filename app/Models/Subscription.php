<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function scopeSearch($query, $value)
    {
        $query->where('name', 'like', '%' . $value . '%')
            ->orWhere('description', 'like', '%' . $value . '%')
            ->orWhere('price', 'like', '%' . $value . '%');
    }

    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function submission()
    {
        return $this->hasOne(FormSubmission::class, 'subscriber_id');
    }
}
