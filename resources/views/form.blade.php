@extends('welcome')

@section('content')
    <div class="row mb-5 mb-xl-10 pt-0 mt-10">
        <div class="card shadow-sm mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true"
                aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bold m-0">Form Name : {{ $subscription->form->name }}</h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Content-->
            <!--begin::Form-->
            <form class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST" action="{{ route('submissionForm', $subscription) }}">
                @csrf
                <!--begin::Card body-->
                <div class="card-body border-top p-9">
                    <!--begin::Input group-->
                    <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-semibold fs-6">Name</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8">
                            <input type="text" name="name" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" value="{{ old('name') }}" required>
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    @foreach ($subscription->form->fields as $field)
                        <!--begin::Input group-->
                        <div class="row mb-6">
                            <!--begin::Label-->
                            <label class="col-lg-4 col-form-label required fw-semibold fs-6">{{ $field->label }}</label>
                            <!--end::Label-->
                            <!--begin::Col-->
                            <div class="col-lg-8">
                                @if (in_array($field->type, ['text', 'email']))
                                    <input type="{{ $field->type }}" name="{{ $field->label }}" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" value="{{ old($field->label) }}"
                                        {{ $field->is_required ? 'required' : '' }}>
                                @elseif(in_array($field->type, ['radio', 'checkbox']))
                                    @foreach ($field->options as $option)
                                        <div class="form-check form-check-custom form-check-solid mb-1">
                                            <input class="form-check-input" type="{{ $field->type }}" name="{{ $field->label }}" value="{{ $option }}"
                                                {{ $field->is_required ? 'required' : '' }} />
                                            <label class="form-check-label">
                                                {{ $option }}
                                            </label>
                                        </div>
                                    @endforeach
                                @else
                                    <select class="form-select form-select-solid" name="{{ $field->label }}" {{ $field->is_required ? 'required' : '' }}>
                                        <option value="{{ null }}">-- Select --</option>
                                        @foreach ($field->options as $option)
                                            <option value="{{ $option }}">{{ $option }}</option>
                                        @endforeach
                                    </select>
                                @endif
                            </div>
                            <!--end::Col-->
                        </div>
                        <!--end::Input group-->
                    @endforeach
                </div>
                <!--end::Card body-->
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
            <!--end::Content-->
        </div>
    </div>
@endsection
