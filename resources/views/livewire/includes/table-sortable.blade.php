<th wire:click="setSortBy('{{ $name }}')">
    {{ $displayName }}
    @if ($sortBy !== $name)
        <i class="ki-outline ki-arrow-up-down"></i>
    @elseif($sortDir == 'desc')
        <i class="ki-outline ki-black-up"></i>
    @else
        <i class="ki-outline ki-black-down"></i>
    @endif
</th>