@extends('layouts.main')

@section('content')
    <div class="row mb-5 mb-xl-10">
        <div class="card mb-5 mb-xl-10">
            <!--begin::Card header-->
            <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true"
                aria-controls="kt_account_profile_details">
                <!--begin::Card title-->
                <div class="card-title m-0">
                    <h3 class="fw-bold m-0">Form Builder</h3>
                </div>
                <!--end::Card title-->
            </div>
            <!--begin::Card header-->
            <!--begin::Content-->
            <!--begin::Form-->
            <form class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST" action="{{ route('form.builder.store', $form) }}">
                @csrf
                <!--begin::Card body-->
                <div class="card-body border-top p-9">
                    <!--begin::Repeater-->
                    <div id="kt_docs_repeater_basic">
                        <!--begin::Form group-->
                        <div class="form-group">
                            <div data-repeater-list="form_builder">
                                <div data-repeater-item>
                                    <div class="form-group row mb-3">
                                        <div class="col-md-2">
                                            <label class="form-label">Type:</label>
                                            <select class="form-select" aria-label="Select Type" name="type">
                                                <option value="{{ null }}">Open this select menu</option>
                                                <option value="text">Text</option>
                                                <option value="email">Email</option>
                                                <option value="checkbox">Checkbox</option>
                                                <option value="radio">RadioButton</option>
                                                <option value="select">Dropdown Select</option>
                                            </select>
                                            @error('form_builder.*.type')
                                                <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label class="form-label">Label:</label>
                                            <input type="text" class="form-control mb-2 mb-md-0" name="label" />
                                            @error('form_builder.*.label')
                                                <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-check form-check-custom form-check-solid mt-2 mt-md-11">
                                                <input class="form-check-input" type="checkbox" value="1" name="is_required" />
                                                <label class="form-check-label" for="form_checkbox">
                                                    Is Required ?
                                                </label>
                                            </div>
                                            @error('form_builder.*.is_required')
                                                <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-3">
                                            <label class="form-label">Options:</label>
                                            <input type="text" class="form-control mb-2 mb-md-0" name="options" />
                                            @error('form_builder.*.options')
                                                <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="col-md-1">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3 mt-md-8">
                                                <i class="ki-duotone ki-trash fs-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span
                                                        class="path5"></span></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Form group-->

                        <!--begin::Form group-->
                        <div class="form-group mt-5">
                            <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                                <i class="ki-duotone ki-plus fs-3"></i>
                                Add
                            </a>
                        </div>
                        <!--end::Form group-->
                    </div>
                    <!--end::Repeater-->
                </div>
                <!--end::Card body-->
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                <!--end::Actions-->
            </form>
            <!--end::Form-->
            <!--end::Content-->
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#kt_docs_repeater_basic').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    </script>
@endpush
