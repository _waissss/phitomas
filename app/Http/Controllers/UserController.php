<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use RealRashid\SweetAlert\Facades\Alert;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function edit(User $user)
    {
        $roles = Role::all();

        return view('user.edit', ['user' => $user, 'roles' => $roles]);
    }

    public function update(Request $request, User $user)
    {
        $request->validate([
            'roles' => 'required'
        ]);
        
        $user->syncRoles($request->roles);

        Alert::success('Success!', 'Successfully change user roles');

        return redirect()->route('user.index');
    }
}
