<?php

namespace App\Http\Controllers;

use App\Models\Subscription;
use App\Models\User;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::count();

        return view('home', ['users' => $users]);
    }

    public function landing()
    {
        $subscriptions = Subscription::all();

        return view('pricing', ['subscriptions' => $subscriptions]);
    }

    public function form(Subscription $subscription)
    {
        $subscription->load('form.fields');

        return view('form', ['subscription' => $subscription]);
    }

    public function submit(Request $request, Subscription $subscription)
    {
        $subscription->submission()->create([
            'form_id' => $subscription->form_id,
            'username' => $request->name,
            'submission_data' => $request->except('name', '_token')
        ]);

        Alert::success('Success', 'You have subscribed to our plan!');

        return redirect()->route('landing');
    }
}
