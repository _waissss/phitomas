@extends('welcome')

@section('content')
    <!--begin::Heading-->
    <div class="mb-13 text-center">
        <h1 class="fs-2hx fw-bold mb-5" id="pricing" data-kt-scroll-offset="{default: 100, lg: 150}">Clear Pricing Makes it
            Easy</h1>
        <div class="text-gray-600 fw-semibold fs-5">Save thousands to millions of bucks by using
            single tool for different
            <br />amazing and outstanding cool and great useful admin
        </div>
    </div>
    <!--end::Heading-->
    <!--begin::Pricing-->
    <div class="text-center" id="kt_pricing">
        <!--begin::Row-->
        <div class="row g-10">
            @foreach ($subscriptions as $sub)
                <!--begin::Col-->
                <div class="col-xl-4">
                    <div class="d-flex h-100 align-items-center">
                        <!--begin::Option-->
                        <div class="w-100 d-flex flex-column flex-center rounded-3 bg-primary py-20 px-10">
                            <!--begin::Heading-->
                            <div class="mb-7 text-center">
                                <!--begin::Title-->
                                <h1 class="text-white mb-5 fw-boldest">{{ $sub->name }}</h1>
                                <!--end::Title-->
                                <!--begin::Description-->
                                <div class="text-white opacity-75 fw-semibold mb-5">
                                    {{ $sub->description }}</div>
                                <!--end::Description-->
                                <!--begin::Price-->
                                <div class="text-center">
                                    <span class="mb-2 text-white">$</span>
                                    <span class="fs-3x fw-bold text-white" data-kt-plan-price-month="199"
                                        data-kt-plan-price-annual="1999">{{ $sub->price }}</span>
                                    <span class="fs-7 fw-semibold text-white opacity-75" data-kt-plan-price-month="/ Mon"
                                        data-kt-plan-price-annual="/ Ann">/ Mon</span>
                                </div>
                                <!--end::Price-->
                            </div>
                            <!--end::Heading-->
                            <!--begin::Features-->
                            <!--begin::Select-->
                            <a href="{{ route('subscriptionForm', $sub) }}"
                                class="btn btn-color-primary btn-active-light-primary btn-light">Select</a>
                            <!--end::Select-->
                        </div>
                        <!--end::Option-->
                    </div>
                </div>
                <!--end::Col-->
            @endforeach
        </div>
        <!--end::Row-->
    </div>
    <!--end::Pricing-->
@endsection
