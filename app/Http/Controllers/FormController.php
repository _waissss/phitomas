<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\FormField;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('form.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('form.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string|min:3'
        ]);

        Form::create($request->all());

        Alert::success('Success!', 'Form created successfully');

        return redirect()->route('form.index');
    }

    /**
     * Display the specified resource.
     */
    public function builder(Form $form)
    {
        return view('form.builder', ['form' => $form]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Form $form)
    {
        return view('form.edit', ['form' => $form]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Form $form)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string|min:3'
        ]);

        $form->update($request->all());

        Alert::success('Success!', 'Form updated successfully');

        return redirect()->route('form.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function storeBuilder(Request $request, Form $form)
    {
        $request->validate([
            'form_builder' => 'required|array|min:1',
            'form_builder.*.type' => 'required|string|min:3',
            'form_builder.*.label' => 'required|string|min:3',
            'form_builder.*.is_required' => 'nullable',
            'form_builder.*.options' => 'nullable|required_if:form_builder.*.type,select,radio,checkbox|string|min:3',
        ]);

        foreach ($request->form_builder as $field) {

            $array = explode(",", $field['options']);

            $form->fields()->create([
                'type' => $field['type'],
                'label' => $field['label'],
                'is_required' => isset($field['is_required']) ? $field['is_required'][0] : 0,
                'options' => $array
            ]);
        }

        Alert::success('Success', 'Form fields created successfully!');

        return redirect()->route('form.index');
    }
}
