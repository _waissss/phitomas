<?php

use App\Http\Controllers\FormController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'landing'])->name('landing');
Route::get('/subscription-form/{subscription}', [HomeController::class, 'form'])->name('subscriptionForm');
Route::post('/subscription-form/{subscription}', [HomeController::class, 'submit'])->name('submissionForm');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');

    Route::middleware('role:admin')->prefix('user')->name('user.')->group(function () {
        Route::get('', [UserController::class, 'index'])->name('index');
        Route::get('/edit/{user}', [UserController::class, 'edit'])->name('edit');
        Route::post('/update/{user}', [UserController::class, 'update'])->name('update');
    });

    Route::middleware('role:admin')->prefix('subscription')->name('subscription.')->group(function () {
        Route::get('', [SubscriptionController::class, 'index'])->name('index');
        Route::get('/create', [SubscriptionController::class, 'create'])->name('create');
        Route::post('/store', [SubscriptionController::class, 'store'])->name('store');
        Route::get('/edit/{subscription}', [SubscriptionController::class, 'edit'])->name('edit');
        Route::put('/update/{subscription}', [SubscriptionController::class, 'update'])->name('update');
    });

    Route::middleware('role:admin')->prefix('form')->name('form.')->group(function () {
        Route::get('', [FormController::class, 'index'])->name('index');
        Route::get('/create', [FormController::class, 'create'])->name('create');
        Route::post('/store', [FormController::class, 'store'])->name('store');
        Route::get('/edit/{form}', [FormController::class, 'edit'])->name('edit');
        Route::put('/update/{form}', [FormController::class, 'update'])->name('update');
        Route::get('/builder/{form}', [FormController::class, 'builder'])->name('builder');
        Route::post('/builder/{form}', [FormController::class, 'storeBuilder'])->name('builder.store');
    });
});