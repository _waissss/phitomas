<?php

namespace App\Http\Controllers;

use App\Models\Form;
use App\Models\Subscription;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('subscription.index');
    }

    public function create()
    {
        $forms = Form::all();

        return view('subscription.create', ['forms' => $forms]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string|min:3',
            'price' => 'required|numeric',
            'form_id' => 'required'
        ]);

        Subscription::create($request->all());

        Alert::success('Success!', 'Subscription plan created successfully');

        return redirect()->route('subscription.index');
    }

    public function edit(Subscription $subscription)
    {
        $forms = Form::all();

        return view('subscription.edit', ['subscription' => $subscription, 'forms' => $forms]);
    }

    public function update(Request $request, Subscription $subscription)
    {
        $request->validate([
            'name' => 'required|string',
            'description' => 'required|string|min:3',
            'price' => 'required|numeric',
            'form_id' => 'required'
        ]);

        $subscription->update($request->all());

        Alert::success('Success!', 'Subscription plan updated successfully');

        return redirect()->route('subscription.index');
    }
}
