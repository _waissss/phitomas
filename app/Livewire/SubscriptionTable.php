<?php

namespace App\Livewire;

use App\Models\Subscription;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class SubscriptionTable extends Component
{
    use WithPagination;

    #[Url(history: true)]
    public $perPage = 5;

    #[Url(history: true)]
    public $search = "";

    #[Url(history: true)]
    public $sortBy = "created_at";

    #[Url(history: true)]
    public $sortDir = "desc";

    protected $listeners = ['deleteSubs'];

    public function updatedSearch()
    {
        $this->resetPage();
    }

    public function setSortBy($sortByField)
    {
        if ($this->sortBy == $sortByField) {
            $this->sortDir = ($this->sortDir == "asc") ? "desc" : "asc";
            return;
        }
        $this->sortBy = $sortByField;
        $this->sortDir = "desc";
    }

    public function render()
    {
        $subscriptions = Subscription::with('form')->search($this->search)->orderBy($this->sortBy, $this->sortDir)->paginate($this->perPage);

        return view('livewire.subscription-table', [
            'subscriptions' => $subscriptions,
            'totalResults' => $subscriptions->total(),
            'firstResult' => $subscriptions->firstItem(),
            'lastResult' => $subscriptions->lastItem()
        ]);
    }

    public function confirmDeleteSub($id)
    {
        $this->dispatch('swal:confirm', type: 'question', text: 'Are you sure you want to delete this subscription plan ?', id: $id, listener: 'deleteSubs');
    }

    public function deleteSubs($id)
    {
        $subscription = Subscription::find($id);

        $subscription->delete();

        $this->dispatch('swal:modal', type: 'success', text: 'Subscription deleted successfully', title: 'Success!');
    }
}
