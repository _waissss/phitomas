@extends('layouts.main')

@section('content')
    <!--begin::Row-->
    <div class="row mb-5 mb-xl-10">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">User List</h3>
            </div>
            <div class="card-body">
                @livewire('user-table')
            </div>
        </div>
    </div>
    <!--end::Row-->
@endsection
