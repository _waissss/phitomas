@extends('layouts.main')

@section('content')
    <!--begin::Row-->
    <div class="row mb-5 mb-xl-10">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">Form List</h3>
                <div class="card-toolbar">
                    <a href="{{ route('form.create') }}" class="btn btn-sm btn-primary">
                        Add
                    </a>
                </div>
            </div>
            <div class="card-body">
                @livewire('form-table')
            </div>
        </div>
    </div>
    <!--end::Row-->
@endsection
