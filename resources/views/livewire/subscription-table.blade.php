<div>
    <div class="d-flex flex-row justify-content-between mb-3">
        <div class="float-left">
            <div class="d-flex flex-lg-row align-items-center">
                <label class="w-auto">
                    <select class="form-select" wire:model.live='perPage'>
                        <option value="5" selected>
                            5
                        </option>
                        <option value="10">
                            10
                        </option>
                        <option value="25">
                            25
                        </option>
                        <option value="50">
                            50
                        </option>
                        <option value="100">
                            100
                        </option>
                    </select>
                </label>
                <small class="ms-2 text-muted">
                    Records per page
                </small>
            </div>
        </div>
        <div class="float-right">
            <input type="text" wire:model.live.debounce.500ms="search" class="form-control" placeholder="Search..." />
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
            <thead>
                <tr class="fw-bold fs-6 text-gray-800 px-7">
                    @include('livewire.includes.table-sortable', ['name' => 'name', 'displayName' => 'Name'])
                    @include('livewire.includes.table-sortable', ['name' => 'description', 'displayName' => 'Description'])
                    @include('livewire.includes.table-sortable', ['name' => 'price', 'displayName' => 'Price'])
                    <th>Form Name</th>
                    @include('livewire.includes.table-sortable', ['name' => 'created_at', 'displayName' => 'Date Joined'])
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($subscriptions as $subscription)
                    <tr>
                        <td>{{ $subscription->name }}</td>
                        <td>{{ $subscription->description }}</td>
                        <td>{{ $subscription->price }}</td>
                        <td>{{ $subscription->form->name }}</td>
                        <td>{{ \Carbon\Carbon::parse($subscription->created_at)->format('j M Y') }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('subscription.edit', $subscription) }}" class='btn btn-icon btn-success btn-sm'><i class="fas fa-pencil"></i></a>
                                <button type="button" class='btn btn-icon btn-danger btn-sm' wire:click="confirmDeleteSub({{ $subscription->id }})"><i class="fas fa-trash"></i></button>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td class="text-center" colspan="6">No Data Available</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <div class="d-flex flex-row justify-content-between mb-3">
        <div class="float-left">
            <span class="text-gray-500">Showing {{ $firstResult }} to {{ $lastResult }} of {{ $totalResults }} results</span>
        </div>
        <div class="float-right">
            {{ $subscriptions->links() }}
            <div class="text-muted">
            </div>
        </div>
    </div>
</div>
