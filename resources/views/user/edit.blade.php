@extends('layouts.main')

@section('content')
    <!--begin::Row-->
    <div class="row mb-5 mb-xl-10">
        <div class="card shadow-sm">
            <div class="card-header">
                <h3 class="card-title">Edit User</h3>
            </div>
            <div class="card-body">
                <!--begin::Form-->
                <form class="form fv-plugins-bootstrap5 fv-plugins-framework" method="POST" action="{{ route('user.update', $user) }}">
                    @csrf
                    <!--begin::Input group-->
                    <div class="row mb-6">
                        <!--begin::Label-->
                        <label class="col-lg-4 col-form-label required fw-semibold fs-6">Roles</label>
                        <!--end::Label-->
                        <!--begin::Col-->
                        <div class="col-lg-8">
                            <select class="form-select" name="roles[]" data-control="select2" data-placeholder="Select roles" multiple>
                                <option></option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->name }}" {{ in_array($role->name,$user->roles()->pluck('name')->toArray())? 'selected': '' }}>{{ $role->name }}</option>
                                @endforeach
                            </select>
                            @error('roles')
                                <div class="fv-plugins-message-container fv-plugins-message-container--enabled invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <!--end::Col-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Actions-->
                    <div class="d-flex justify-content-end py-6 px-9">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <!--end::Row-->
@endsection
