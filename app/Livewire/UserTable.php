<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class UserTable extends Component
{
    use WithPagination;

    #[Url(history: true)]
    public $perPage = 5;

    #[Url(history: true)]
    public $search = "";

    #[Url(history: true)]
    public $sortBy = "created_at";

    #[Url(history: true)]
    public $sortDir = "desc";

    public function updatedSearch()
    {
        $this->resetPage();
    }

    public function setSortBy($sortByField)
    {
        if ($this->sortBy == $sortByField) {
            $this->sortDir = ($this->sortDir == "asc") ? "desc" : "asc";
            return;
        }
        $this->sortBy = $sortByField;
        $this->sortDir = "desc";
    }

    public function render()
    {
        $users = User::with('roles')->search($this->search)->orderBy($this->sortBy, $this->sortDir)->paginate($this->perPage);

        return view('livewire.user-table', [
            'users' => $users,
            'totalResults' => $users->total(),
            'firstResult' => $users->firstItem(),
            'lastResult' => $users->lastItem()
        ]);
    }
}
