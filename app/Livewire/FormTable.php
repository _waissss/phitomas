<?php

namespace App\Livewire;

use App\Models\Form;
use Livewire\Attributes\Url;
use Livewire\Component;
use Livewire\WithPagination;

class FormTable extends Component
{
    use WithPagination;

    #[Url(history: true)]
    public $perPage = 5;

    #[Url(history: true)]
    public $search = "";

    #[Url(history: true)]
    public $sortBy = "created_at";

    #[Url(history: true)]
    public $sortDir = "desc";

    protected $listeners = ['deleteForm', 'deleteFormBuilder'];

    public function updatedSearch()
    {
        $this->resetPage();
    }

    public function setSortBy($sortByField)
    {
        if ($this->sortBy == $sortByField) {
            $this->sortDir = ($this->sortDir == "asc") ? "desc" : "asc";
            return;
        }
        $this->sortBy = $sortByField;
        $this->sortDir = "desc";
    }

    public function render()
    {
        $forms = Form::search($this->search)->orderBy($this->sortBy, $this->sortDir)->paginate($this->perPage);

        return view('livewire.form-table', [
            'forms' => $forms,
            'totalResults' => $forms->total(),
            'firstResult' => $forms->firstItem(),
            'lastResult' => $forms->lastItem()
        ]);
    }

    public function confirmDeleteForm($id)
    {
        $this->dispatch('swal:confirm', type: 'question', text: 'Are you sure you want to delete this form ? It will automatically deleted the associated subscription.', id: $id, listener: 'deleteForm');
    }

    public function confirmDeleteFormBuilder($id)
    {
        $this->dispatch('swal:confirm', type: 'question', text: 'Are you sure you want to delete this form builder?', id: $id, listener: 'deleteFormBuilder');
    }

    public function deleteForm($id)
    {
        $form = Form::find($id);

        if ($form->subscription()->exists()) {
            $form->subscription()->delete();
        }

        $form->delete();

        $this->dispatch('swal:modal', type: 'success', text: 'Form deleted successfully', title: 'Success!');
    }

    public function deleteFormBuilder($id)
    {
        $form = Form::find($id);

        $form->fields()->delete();

        $this->dispatch('swal:modal', type: 'success', text: 'Form builder deleted successfully', title: 'Success!');
    }
}
